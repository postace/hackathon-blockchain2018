import React from 'react';
import { View } from 'native-base';

const withDimensions = (options = {}) => Component =>
  class extends React.Component {
    state = {
      dimensions: undefined
    };

    onLayout(e) {
      if (this.state.dimensions) {
        return;
      }
      let { width, height } = e.nativeEvent.layout;
      this.setState({ dimensions: { width, height } });
    }

    render() {
      const { dimensions } = this.state;
      const { style } = options;
      return (
        <View
          onLayout={e => this.onLayout(e)}
          style={{
            flex: 1,
            ...style
          }}>
          {dimensions && <Component {...this.props} dimensions={dimensions} />}
        </View>
      );
    }
  };

export default withDimensions;
