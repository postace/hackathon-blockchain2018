import { Alert } from 'react-native';

let failedNetworkAlerted = false;

export default ({
  type,
  payload,
  process,
  onBeforeStart,
  onBeforeSuccess,
  onAfterSuccess,
  onError,
  onNetworkFailedConfirm
}) => {
  const startAction = () => ({ type: `${type}_START`, payload });
  const successAction = data => ({
    type: `${type}_SUCCESS`,
    payload: data
  });
  const failAction = error => ({ type: `${type}_FAIL`, error });

  const action = async (dispatch, getState, objDeps) => {
    if (onBeforeStart) {
      await onBeforeStart({ dispatch });
    }

    dispatch(startAction());

    try {
      let data = await process(payload);

      if (onBeforeSuccess) {
        await onBeforeSuccess({ payload: data });
      }

      dispatch(successAction(data));

      if (onAfterSuccess) {
        await onAfterSuccess({ payload: data, dispatch, getState, ...objDeps, params: payload });
      }
    } catch (e) {
      // console.log(e);
      if (e.message === 'Network error: Network request failed' && !failedNetworkAlerted) {
        failedNetworkAlerted = true;
        const buttons = [];
        if (onNetworkFailedConfirm) {
          buttons.push({ text: 'Cancel', onPress: () => (failedNetworkAlerted = false) });
          buttons.push({
            text: 'Retry',
            onPress: async () => {
              failedNetworkAlerted = false;
              await onNetworkFailedConfirm({ dispatch });
            }
          });
        } else {
          buttons.push({ text: 'OK', onPress: () => (failedNetworkAlerted = false) });
        }
        Alert.alert('Error', 'Network request failed', buttons, { cancelable: false });
      }

      if (onError) {
        await onError({ error: e, dispatch });
      }

      return dispatch(failAction(e));
    }
  };

  return action;
};
