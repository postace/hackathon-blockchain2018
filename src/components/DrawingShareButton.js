import { tap } from 'dike/utils/sounds';
import React from 'react';

import { Share } from 'react-native';
import { Button, Text } from 'native-base';

const onSharePress = drawing => {
  const numbers = drawing.userBets.map(b => b.number).join(', ');

  const title = `Drawing #${drawing.serial}`;
  const message = `
    Drawing #${drawing.serial}'s result has finally come out.
    I placed  my bets on ${drawing.userBets.length} numbers ${numbers}.
    The result is ${drawing.result.join(', ')}.`;

  Share.share({ message, title });
};

export default ({ drawing }) => (
  <Button
    large
    block
    onPress={() => {
      onSharePress(drawing);
      tap.play();
    }}>
    <Text>Share</Text>
  </Button>
);
