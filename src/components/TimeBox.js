import moment from 'moment';
import React from 'react';

import { StyleSheet } from 'react-native';
import { View, Text, H3 } from 'native-base';

const styles = StyleSheet.create({
  container: { alignItems: 'center' }
});

export default ({ time }) => {
  const ts = moment(time);
  return (
    <View style={styles.container}>
      <H3 bold>{ts.format('HH:mm:ss')}</H3>
      <Text>{ts.format('MM/DD/YYYY')}</Text>
    </View>
  );
};
