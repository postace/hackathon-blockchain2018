import React from 'react';
import { Text } from 'native-base';

export default () => (
  <Text center style={{ marginTop: 60 }}>
    No history recorded
  </Text>
);
