import React from 'react';
import ScreenModal from 'dike/components/ScreenModal';

import { View, Button, Text } from 'native-base';
import { tap } from 'dike/utils/sounds';

export default ({ onDismiss, onSubmit, title = 'Are you sure?', notice }) => (
  <ScreenModal title={title} onDismiss={onDismiss}>
    <Text padVertical>{notice}</Text>
    <View marginAround textAround>
      <Button
        success
        fixedWidth
        onPress={() => {
          onDismiss();
          tap.play();
        }}>
        <Text>No</Text>
      </Button>
      <Button fixedWidth onPress={() => onSubmit()}>
        <Text>Yes</Text>
      </Button>
    </View>
  </ScreenModal>
);
