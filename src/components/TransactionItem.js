import theme from 'dike/theme';
import React from 'react';
import moment from 'moment';
import BigNumber from 'bignumber.js';

import { StyleSheet } from 'react-native';
import { View, Text, ListItem, Left, Body, Right, H3, H2 } from 'native-base';

import DikeNumber from 'dike/components/DikeNumber';

const descriptions = {
  demoToken: 'Register bonus',
  winning: 'Win a bet',
  placeBet: 'Place a bet',
  resetBalance: 'Reset balance'
};

const styles = StyleSheet.create({
  customIcon: { height: '100%', borderLeftWidth: 6 },
  persistText: { textAlign: 'right', width: theme.spacingUnit * 24 }
});

export default ({ onPress, transaction }) => {
  const amount = new BigNumber(transaction.amount);
  return (
    <ListItem icon flat onPress={() => onPress(transaction)}>
      <Left>
        <View
          style={[
            styles.customIcon,
            {
              borderLeftColor: amount.gt(0) ? theme.dikeNumber.normal : theme.dikeNumber.special
            }
          ]}
        />
      </Left>
      <Body>
        <View>
          <H2>{descriptions[transaction.data.type]}</H2>
          <Text>{moment(transaction.date).fromNow()}</Text>
        </View>
      </Body>
      <Right>
        <View horizontal center revert>
          {transaction.data.type === 'winning' && (
            <DikeNumber
              number={transaction.data.number}
              special={!!transaction.data.special}
              size={40}
            />
          )}
          <H3
            bold
            style={[
              styles.persistText,
              {
                color: amount.gt(0) ? theme.dikeNumber.normal : theme.dikeNumber.special
              }
            ]}>
            {`${amount.gt(0) ? '+' : ''}${amount.toFormat(0)}`}
          </H3>
        </View>
      </Right>
    </ListItem>
  );
};
