import moment from 'moment';
import * as types from './drawing.types';
import { SIGN_OUT } from 'dike/store/user/user.types';

const initialState = {
  drawingStatus: {
    inProgress: false
  },
  currentDrawing: null,
  drawingList: [],
  drawingDetail: {},
  lastBet: null,
  drawingHistory: {
    date: moment(),
    participated: false,
    drawings: null
  },
  previousDrawing: null,
  drawingChart: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.REMOVE_CURRENT_DRAWING:
      return {
        ...state,
        drawingStatus: { ...state.drawingStatus, inProgress: false },
        currentDrawing: null
      };
    case `${types.GET_LAST_BET}_SUCCESS`:
      return { ...state, lastBet: { ...action.payload } };
    case `${types.GET_DRAWING_DETAIL}_SUCCESS`:
      return { ...state, drawingDetail: { ...action.payload } };
    case `${types.GET_DRAWING_HISTORY}_SUCCESS`:
      return {
        ...state,
        drawingHistory: { ...state.drawingHistory, drawings: action.payload }
      };
    case `${types.GET_DRAWING_STATUS}_SUCCESS`:
      return {
        ...state,
        drawingStatus: { inProgress: action.payload.inProgress, nextAt: action.payload.nextAt },
        currentDrawing: action.payload.currentDrawing
      };
    case `${types.GET_PREVIOUS_DRAWING}_SUCCESS`:
      return {
        ...state,
        previousDrawing: { ...action.payload }
      };

    case types.SET_DRAWING_HISTORY_PROPS:
      return { ...state, drawingHistory: { ...state.drawingHistory, ...action.payload } };
    case types.SET_CURRENT_DRAWING_STATS:
      return {
        ...state,
        currentDrawing: {
          ...state.currentDrawing,
          drawingStats: { ...state.currentDrawing.drawingStats, ...action.payload }
        }
      };
    case types.SET_USER_BETS:
      return {
        ...state,
        currentDrawing: {
          ...state.currentDrawing,
          userBets: [...state.currentDrawing.userBets, ...action.payload],
          participated: true
        }
      };
    case `${types.GET_DRAWING_CHART}_SUCCESS`:
      return { ...state, drawingChart: action.payload };
    case SIGN_OUT:
      return { ...initialState };
    default:
      return state;
  }
};
