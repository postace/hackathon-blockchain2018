import * as types from './app.types';

import { setToken } from 'dike/utils/graphClients';
import { getStorageItem } from 'dike/utils/asyncStorage';

export const setLoginScreenLoading = value => ({
  type: types.SET_LOGIN_SCREEN_LOADING,
  payload: value
});

export const setRegisterScreenLoading = value => ({
  type: types.SET_REGISTER_SCREEN_LOADING,
  payload: value
});

export const setAppRefreshing = value => ({ type: types.SET_APP_REFRESHING, value });

export const refreshApp = () => async (dispatch, getState, { socketMiddleware }) => {
  const state = getState();
  const isAppRefreshing = state.app.isAppRefreshing;

  if (isAppRefreshing) {
    return;
  }
  dispatch(setAppRefreshing(true));

  let userToken;
  //BUG: with hot reload
  userToken = await getStorageItem('userToken');
  if (userToken) {
    setToken(userToken);
  }
  const isLoggedIn = !!state.user._id;
  if (isLoggedIn) {
    let socketInstance = socketMiddleware.getInstance();
    if (!socketInstance.connected) {
      socketMiddleware.reconnect(dispatch, getState);
    }
  }

  dispatch(setAppRefreshing(false));
};

export const setTransactionsScreenLoading = value => ({
  type: types.SET_TRANSACTION_HISTORY_SCREEN_LOADING,
  payload: value
});

export const setWalletScreenLoading = value => ({
  type: types.SET_WALLET_SCREEN_LOADING,
  payload: value
});

export const setLastBetLoading = value => ({ type: types.SET_LAST_BET_LOADING, payload: value });

export const setDrawingDetailLoading = value => ({
  type: types.SET_DRAWING_DETAIL_LOADING,
  payload: value
});

export const setDrawingHistoryLoading = value => ({
  type: types.SET_DRAWING_HISTORY_LOADING,
  payload: value
});

export const setPlaceBetsScreenLoading = value => ({
  type: types.SET_PLACE_BETS_SCREEN_LOADING,
  payload: value
});

export const setPreviousDrawingLoading = value => ({
  type: types.SET_PREVIOUS_DRAWING_LOADING,
  payload: value
});

export const setPlaceBetsPopupProps = props => ({
  type: types.SET_PLACE_BETS_POPUP_PROPS,
  payload: props
});

export const setWalletPopupProps = props => ({
  type: types.SET_WALLET_POPUP_PROPS,
  payload: props
});

export const setVerifyEmailScreenLoading = value => ({
  type: types.SET_VERIFY_EMAIL_SCREEN_LOADING,
  payload: value
});

export const setForgotPasswordScreenProps = props => ({
  type: types.SET_FORGOT_PASSWORD_SCREEN_PROPS,
  payload: props
});

export const setChangePasswordScreenProps = props => ({
  type: types.SET_CHANGE_PASSWORD_SCREEN_PROPS,
  payload: props
});

export const setReportIssueScreenLoading = value => ({
  type: types.SET_REPORT_ISSUE_SCREEN_LOADING,
  payload: value
});

export const setDikeResultPopupProps = props => ({
  type: types.SET_DIKE_RESULT_POPUP_PROPS,
  payload: props
});

export const setPeopleScreenLoading = value => ({
  type: types.SET_PEOPLE_SCREEN_LOADING,
  payload: value
});
