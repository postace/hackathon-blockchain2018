import gql from 'graphql-tag';
import { userGraphClient as client } from 'dike/utils/graphClients';

export default {
  register: async ({ username, email, password, refererCode }) => {
    const mutate = gql`
      mutation {
        createUser(user: {
          username: "${username}"
          email: "${email.trim().toLowerCase()}"
          password: "${password}"
          ${refererCode ? `refererCode: "${refererCode}"` : ''}
        })
        {
          token
        }
      }
    `;
    const result = await client.mutate({ mutation: mutate });
    return result.data.createUser;
  },

  login: async ({ email, password }) => {
    const query = gql`
      query {
        login (user: {
          email: "${email.trim().toLowerCase()}"
          password: "${password}"
        })
        {
          token
        }
      }
    `;
    const result = await client.query({ query });
    return result.data.login;
  },

  me: async () => {
    const query = gql`
      query {
        me {
          _id
          username
          email
          ethDepositAddress
          emailVerified
          activated
          profilePicture
          ethPrivateKey
          displayName
          allowMember
          allowService
          balance(asset: "dike")
          transactions(limit: 5) {
            _id
            amount
            date
            data {
              type
              numbers
              drawingId
              special
              number
            }
          }
        }
      }
    `;
    const result = await client.query({ query });
    return result.data.me;
  },

  getLastTransactions: async () => {
    const query = gql`
      query {
        transactions(limit: 5) {
          _id
          amount
          date
          data {
            type
            numbers
            drawingId
            special
            number
          }
        }
      }
    `;

    const result = await client.query({ query });
    return result.data.transactions;
  },

  getBalance: async ({ asset }) => {
    const query = gql`
      query {
        balance(asset: "${asset}")
      }
    `;

    const result = await client.query({ query });
    return result.data.balance;
  },

  getTransactionHistory: async ({ fromDate, toDate }) => {
    const query = gql`
      query {
        transactions(fromDate: ${fromDate}, toDate: ${toDate}){
          _id
          amount
          date
          data {
            type
            numbers
            drawingId
            special
            number
          }
        }
      }
    `;

    const result = await client.query({ query });
    return result.data.transactions;
  },

  resetDemoBalance: async () => {
    const mutate = gql`
      mutation {
        resetDemoBalance
      }
    `;

    const result = await client.mutate({ mutation: mutate });
    return result.data.resetDemoBalance;
  },

  verifyEmail: async code => {
    const mutate = gql`
      mutation {
        verfiyEmail(code: "${code}")
      }
    `;

    const result = await client.mutate({ mutation: mutate });
    return result.data.verfiyEmail;
  },

  resendVerifyCode: async () => {
    const mutate = gql`
      mutation {
        resendVerifyEmail
      }
    `;

    const result = await client.mutate({ mutation: mutate });
    return result.data.resendVerifyEmail;
  },

  sendForgotPasswordEmail: async email => {
    const mutate = gql`
      mutation {
        sendForgotPasswordEmail(email: "${email}")
      }
    `;
    const result = await client.mutate({ mutation: mutate });
    return result.data.sendForgotPasswordEmail;
  },

  setPassword: async ({ email, password, code }) => {
    const mutate = gql`
      mutation {
        setPassword(email: "${email}", password: "${password}", code: "${code}")
      }
    `;
    const result = await client.mutate({ mutation: mutate });
    return result.data.setPassword;
  },

  changePassword: async ({ oldPassword, newPassword }) => {
    const mutate = gql`
      mutation {
        changePassword(oldPassword:"${oldPassword}", newPassword: "${newPassword}")
      }
    `;
    const result = await client.mutate({ mutation: mutate });
    return result.data.changePassword;
  },

  reportIssue: async ({ title, description }) => {
    const mutate = gql`
      mutation {
        reportIssue(title: "${title}", description: "${description}")
      }
    `;
    const result = await client.mutate({ mutation: mutate });
    return result.data.reportIssue;
  },

  getRanking: async () => {
    const query = gql`
      query {
        ranking {
          username
          point
        }
      }
    `;
    const result = await client.query({ query });
    return result.data.ranking;
  },

  editProfile: async ({ displayName, profilePicture }) => {
    const mutate = gql`
      mutation {
        editProfile(displayName: "${displayName}", profilePicture: "${profilePicture}")
      }    
    `;
    const result = await client.mutate({ mutation: mutate });
    return result.data.editProfile;
  },

  fetchUserData: async ({ userId }) => {
    const query = gql`
      query {
        getUser(id: "${userId}") {
          _id
          profilePicture
          displayName
          ethDepositAddress
          activeCode
          emailVerified
        }
      }
    `;
    const result = await client.query({ query });
    return result.data.getUser;
  },

  fetchAllNearby: async ({ userIds }) => {
    const query = gql`
    query{
      nearby(ids: ${userIds}){
        _id
        username
        email
        profilePicture
        displayName
        ethDepositAddress
        activated
        activeCode
        emailVerified
        allowMember
        allowService
      }
    }`;
    const result = await client.query({ query });
    return result.data.nearby;
  },

  updatePrivacyRule: async ({ allowMember, allowService }) => {
    const mutate = gql`
    mutation{
      editPrivacy(allowMember: ${allowMember}, allowService: ${allowService})
    }`;
    const result = await client.mutate({ mutation: mutate });
    return result.data.editPrivacy;
  },

  sendPointsLocal: async ({ address, amount, privateKey }) => {
    try {
      const { CoinType, EthWallet, InfinitoApi } = require('node-infinito-wallet');
      let walletConfig = {
        coinType: CoinType.ETH.symbol,
        isTestNet: true,
        privateKey: Buffer.from(privateKey, 'hex')
      };

      let api = new InfinitoApi({
        apiKey: '2412e120-4e07-43b3-9f06-e42e9d837a8e',
        secret: 'zntpAF25OyFp05uH8VL7b1gsnQGhfSFrVHOqh4R9vF5PKUCHgzp6Vc5577T3ZfZw',
        baseUrl: 'https://staging-api-testnet.infinitowallet.io',
        logLevel: 'WARN'
      });

      let wallet = new EthWallet(walletConfig);
      wallet.setApi(api);

      let contractAddress = '0x54184ec74decba1620da48004d4fa45e82f68c11';
      let to = address;
      let txParams = {};
      txParams.sc = {};
      txParams.sc.contractAddress = contractAddress;
      txParams.sc.nameFunc = 'transfer';
      txParams.sc.typeParams = ['address', 'uint256'];
      txParams.sc.paramsFuncs = [to, amount];

      let rawTx = await wallet.createRawTx(txParams);
      let result = await wallet.send({
        rawTx: rawTx,
        isBroadCast: true
      });
      return result.tx_id;
    } catch (err) {
      console.dir(err);
    }
  }

  sendPoints: async ({ address, amount, privateKey }) => {
    const mutate = gql`
    mutation{
      giveCredit(address: "${address}", amount: ${parseInt(amount)})
    }`;
    const result = await client.mutate({ mutation: mutate });
    return result.data.giveCredit;
  }
};
