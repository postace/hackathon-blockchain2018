export default {
  primary: {
    blur: '#666666',
    main: '#222222',
    semiDark: '#333333',
    dark: '#111111'
  },
  text: {
    placeholder: '#8da0bb',
    feather: '#598399',
    blur: '#c5dbff',
    main: '#fff',
    highlight: '#1ee1f6',
    important: '#f8f006',
    error: '#ff4e1e',
    contrast: '#000'
  },
  button: {
    primary: '#ff4e1e',
    info: '#666666'
  },
  bgGradient: {
    start: '#333333',
    end: '#111111'
  },
  bbGradient: {
    start: '#222222',
    end: '#444444'
  },
  dikeNumber: {
    normal: '#67c630',
    special: '#ff4e1e'
  },
  spacingUnit: 5,
  fontSizeBase: 15,
  inputFontSize: 17,
  inputBorderColor: '#D9D5DC',
  inputErrorBorderColor: '#ed2f2f'
};
