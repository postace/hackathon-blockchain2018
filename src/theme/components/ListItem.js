import palette from '../palette';

export default variables => {
  return {
    '.flat': {
      paddingVertical: 0,
      marginLeft: 0,
      alignItems: 'center',
      'NativeBase.Right': {
        paddingRight: 0,
        height: palette.spacingUnit * 12
      },
      'NativeBase.Body': {
        height: palette.spacingUnit * 12
      },
      height: palette.spacingUnit * 12
    }
  };
};
