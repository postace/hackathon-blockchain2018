import React from 'react';
import { Content } from 'native-base';

import PeopleListScreen from 'dike/screen/people/PeopleListScreen.container';

import DikeBackground from 'dike/components/DikeBackground';
import UserInfo from 'dike/components/UserInfo';

export default ({ navigation, currentDrawingId }) => (
  <DikeBackground>
    <Content>
      <UserInfo
        ViewProps={{
          paddingContent: true
        }}
      />
      <PeopleListScreen navigation={navigation} />
    </Content>
  </DikeBackground>
);
