import { connect } from 'react-redux';
import EditProfileScreen from './EditProfileScreen.component.js';

import { editUserProfile } from 'dike/store/user/user.actions';

export default connect(({ user }) => ({
  displayName: user.displayName,
  profilePicture: user.profilePicture
}), dispatch => ({
  onEditProfile(displayName, profilePicture, success) {
    dispatch(editUserProfile({displayName, profilePicture}, () =>  success && success()));
  }
}))(EditProfileScreen);