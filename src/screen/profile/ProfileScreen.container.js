import { connect } from 'react-redux';
import ProfileScreen from './ProfileScreen.component';

export default connect(({ drawing }) => ({
  currentDrawingId: drawing.currentDrawing ? drawing.currentDrawing.id : null
}))(ProfileScreen);
