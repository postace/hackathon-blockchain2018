import React, { Component } from 'react';
import { TouchableOpacity, Keyboard, TouchableWithoutFeedback } from 'react-native';
import { View, Input, Thumbnail, Form, Button, Text, Item } from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import DikeBackground from 'dike/components/DikeBackground';

export default class EditProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayName: props.displayName,
      profilePicture: props.profilePicture
    }
  }

  onChooseAvatar() {
    ImagePicker.openPicker({
      multiple: false,
      cropping: true,
      mediaType: "photo",
      includeBase64: true,
      cropperCircleOverlay: true,
      compressImageMaxWidth: 300,
      compressImageMaxHeight: 300
    }).then(image => {
      let data = `data:${image.mime};base64,${image.data}`;
      this.setState({ profilePicture: data });
    }).catch(err => {
      // nothing to do
    });
  }

  handleSubmit() {
    const { displayName, profilePicture } = this.state;
    this.props.onEditProfile(displayName, profilePicture, () => this.props.navigation.goBack());
  }

  render() {
    const { displayName, profilePicture } = this.state;

    return (
      <DikeBackground>
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View full center>
            <View authScreenWrapper center style={{marginBottom: 20}}>
              <TouchableOpacity onPress={() => this.onChooseAvatar()} style={{marginBottom: 10}}>
                { profilePicture &&
                  <Thumbnail large source={{uri: profilePicture}} />}
                {!profilePicture &&
                  <Thumbnail large source={require('dike/asset/default_avatar.png')} />
                }
              </TouchableOpacity>
              <Text endDescription>
                Press on the icon above to change your profile picture
              </Text>
            </View>
            <View authScreenWrapper>
              <Form full>
                <Item noLabel>
                  <Input
                    placeholder="Display Name"
                    value={displayName}
                    autoCapitalize="none"
                    onChangeText={v => this.setState({ displayName: v })}>
                  </Input>
                </Item>
              </Form>

              <View fullWidth center>
                <View half fullFormFooter>
                  <Button
                    block
                    primary
                    onPress={() => {
                      this.handleSubmit();
                    }}>
                    <Text>Submit</Text>
                  </Button>
                </View>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </DikeBackground>
    );
  }
}