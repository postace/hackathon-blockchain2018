import theme from 'dike/theme';
import React, { Component } from 'react';
import { tap } from 'dike/utils/sounds';

import { FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import { Text, CheckBox, View } from 'native-base';

import DikeBackground from 'dike/components/DikeBackground';
import DateSelector from 'dike/components/DateSelector';
import NoHistoryNotice from 'dike/components/NoHistoryNotice';
import ScreenLoading from 'dike/components/ScreenLoading';

import DrawingHistoryItem from './DrawingHistoryItem.component';

const styles = StyleSheet.create({
  filterer: { alignItems: 'center' },
  padCheckbox: { marginLeft: theme.spacingUnit * 3 }
});

export default class DrawingHistoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { expandedDrawingId: null };
  }

  componentDidMount() {
    const { participated, drawings, date, onGetDrawingHistory } = this.props;
    onGetDrawingHistory({ date, participated });
  }

  renderNotice() {
    const { drawings, isLoading } = this.props;
    if (!isLoading) {
      if (!drawings || drawings.length < 1) {
        return <NoHistoryNotice />;
      }
    }
  }

  handleToggleDrawing(drawing) {
    if (drawing.id === this.state.expandedDrawingId) {
      this.setState({ expandedDrawingId: null });
    } else {
      this.setState({ expandedDrawingId: drawing.id });
    }
  }

  render() {
    const { isLoading, drawings, date, participated, onGetDrawingHistory, navigation } = this.props;
    return (
      <DikeBackground>
        <View textBetween marginAround style={styles.filterer}>
          <DateSelector
            defaultValue={date}
            onChange={newDate => onGetDrawingHistory({ date: newDate, participated })}
          />
          <View horizontal>
            <CheckBox
              color={theme.text.highlight}
              checked={participated}
              onPressIn={() => {
                onGetDrawingHistory({ date, participated: !participated });
                tap.play();
              }}
            />
            <TouchableOpacity
              onPressIn={() => {
                onGetDrawingHistory({ date, participated: !participated });
                tap.play();
              }}>
              <Text highlight style={styles.padCheckbox}>
                Participated
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {isLoading && <ScreenLoading />}
        {this.renderNotice()}
        {!isLoading &&
          drawings &&
          drawings.length > 0 && (
            <FlatList
              extraData={this.state}
              keyExtractor={item => item.id}
              data={drawings}
              renderItem={({ item }) => (
                <DrawingHistoryItem
                  onPress={drawing => {
                    this.handleToggleDrawing(drawing);
                    tap.play();
                  }}
                  onNavigate={drawing => {
                    navigation.navigate('DrawingDetail', { drawingId: drawing.id });
                    tap.play();
                  }}
                  drawing={item}
                  expandId={this.state.expandedDrawingId}
                />
              )}
            />
          )}
      </DikeBackground>
    );
  }
}
