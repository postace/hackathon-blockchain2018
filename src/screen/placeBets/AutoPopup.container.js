import { connect } from 'react-redux';
import DikeAutoPopup from 'dike/components/DikeAutoPopup';

import { setPlaceBetsPopupProps } from 'dike/store/app/app.actions';

export default connect(
  ({ app }) => ({ ...app.placeBetsPopup }),
  dispatch => ({
    onDismiss() {
      dispatch(setPlaceBetsPopupProps({ isVisible: false }));
    }
  })
)(DikeAutoPopup);
