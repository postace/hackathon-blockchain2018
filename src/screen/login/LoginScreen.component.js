import theme from 'dike/theme';
import { tap } from 'dike/utils/sounds';
import React, { Component } from 'react';

import { Dimensions, Image, StyleSheet, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { View, Text, Form, Item, Input, Label, Button, Spinner } from 'native-base';
import TextButton from 'dike/components/TextButton';

const styles = StyleSheet.create({
  background: { position: 'relative' },
  mainScreen: { position: 'absolute' },
  logo: { marginBottom: theme.spacingUnit * 4 }
});

const { width, height } = Dimensions.get('window');

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  render() {
    const { email, password } = this.state;
    const { navigation, onLogin, loginError, onClearError, isLoading } = this.props;

    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={{ width, height }}>
          <View style={[{ width, height }, styles.background]} key="background">
            <Image style={{ width, height }} source={require('dike/asset/LoginBg.jpg')} />
          </View>,
          <View full center style={styles.mainScreen} key="screen">
            <View authScreenWrapper center>
              <Image
                source={require('dike/asset/aura_logo.png')}
                style={[{ width: width / 2, height: width / 2 }, styles.logo]}
              />

              {loginError && <Text error small center>{`*${loginError}`}</Text>}

              {!isLoading && [
                <Form full key="form">
                  <Item floatingLabel>
                    <Label>Email</Label>
                    <Input
                      keyboardType="email-address"
                      value={email}
                      autoCapitalize="none"
                      onChangeText={v => this.setState({ email: v })}
                    />
                  </Item>
                  <Item floatingLabel>
                    <Label>Password</Label>
                    <Input
                      secureTextEntry={true}
                      value={password}
                      autoCapitalize="none"
                      onChangeText={v => this.setState({ password: v })}
                    />
                  </Item>
                </Form>,
                <View fullFormFooter half key="btns">
                  <Button
                    block
                    primary
                    onPress={() => {
                      onClearError();
                      onLogin({ email, password });
                      tap.play();
                    }}>
                    <Text>Sign in</Text>
                  </Button>
                  <Button
                    block
                    info
                    onPress={() => {
                      navigation.navigate('Register');
                      tap.play();
                    }}>
                    <Text>Register</Text>
                  </Button>
                  <TextButton
                    TextProps={{ small: true, italic: true }}
                    text="Forgot Password?"
                    onPress={() => {
                      navigation.navigate('ForgotPassword');
                      tap.play();
                    }}
                  />
                </View>
              ]}
            </View>

            {isLoading && (
              <View style={{ height: 250 }} center>
                <Spinner />
              </View>
            )}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
