import { connect } from 'react-redux';
import GameScreen from './GameScreen.component';

export default connect(({ drawing }) => ({
  drawing: drawing.currentDrawing
}))(GameScreen);
