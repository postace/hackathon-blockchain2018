import React, { Component } from 'react';
import { View, Text, Card, CardItem, H3, Left, Right } from 'native-base';

import TimeBox from 'dike/components/TimeBox';
import DikeResult from 'dike/components/DikeResult';
import CardLoading from 'dike/components/CardLoading';

export default class PreviousDrawingCard extends Component {
  componentDidMount() {
    const { drawing, onGetDrawing } = this.props;
    if (!drawing) {
      onGetDrawing();
    }
  }

  render() {
    const { drawing, isLoading, onNavigate } = this.props;
    if (isLoading) {
      return <CardLoading />;
    }

    return (
      <Card bgSemiDark>
        <CardItem header borderBottom>
          <Left>
            <H3 bold>Previous Drawing</H3>
          </Left>
          <Right>
            <TimeBox time={drawing.endTime} />
          </Right>
        </CardItem>
        <CardItem>
          <DikeResult result={drawing.result} />
        </CardItem>
        <CardItem endFlexBody footer button onPress={() => onNavigate && onNavigate(drawing)}>
          <View>
            <Text highlight>View Detail</Text>
          </View>
        </CardItem>
      </Card>
    );
  }
}
