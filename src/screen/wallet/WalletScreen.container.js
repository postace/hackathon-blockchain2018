import { connect } from 'react-redux';
import WalletScreen from './WalletScreen.component';

import { resetDemoBalance } from 'dike/store/user/user.actions';

export default connect(
  ({ app, user }) => ({
    isLoading: app.isWalletScreenLoading,
    activated: user.activated
  }),
  dispatch => ({
    onResetDemoBalance() {
      dispatch(resetDemoBalance());
    }
  })
)(WalletScreen);
