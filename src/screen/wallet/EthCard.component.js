import React from 'react';
import QRCode from 'react-native-qrcode';

import { StyleSheet } from 'react-native';
import { View, Text, Card, CardItem, H3, Left, Right } from 'native-base';

const styles = StyleSheet.create({
  qrCode: { backgroundColor: 'white', padding: 10 }
});

export default ({ ethDepositAddress }) => (
  <Card bgMain>
    <CardItem header>
      <H3 bold>Deposit/Withdraw Eth</H3>
    </CardItem>
    <CardItem>
      <Left>
        <View style={styles.qrCode}>
          <QRCode value={ethDepositAddress} bgColor="black" fgColor="white" />
        </View>
      </Left>
      <Right>
        <Text>This feature will not available in Dike's demo version</Text>
      </Right>
    </CardItem>
  </Card>
);
