import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Text, Button, Card, CardItem, H3, Body } from 'native-base';

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(49, 58, 85, 0.7)',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
});

export default ({ onResetPress, activated }) => (
  <Card bgMain>
    <CardItem header>
      <H3 bold>Reset</H3>
    </CardItem>
    <CardItem>
      <Body>
        <Text>During demo period, you can reset your balance back to Đ1,000 dike anytime.</Text>
        <Text>To reset your balance, press the reset button below.</Text>
      </Body>
    </CardItem>
    <CardItem footer center>
      <View half>
        <Button block onPress={() => onResetPress()}>
          <Text>Reset</Text>
        </Button>
      </View>
    </CardItem>
    {!activated && <View style={styles.overlay} />}
  </Card>
);
