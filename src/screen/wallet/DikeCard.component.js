import React from 'react';
import { H3, Text, Card, CardItem } from 'native-base';

export default () => (
  <Card bgMain>
    <CardItem header>
      <H3 bold>Buy/Sell Dike</H3>
    </CardItem>
    <CardItem>
      <Text>This feature will not available in Dike's demo version</Text>
    </CardItem>
  </Card>
);
