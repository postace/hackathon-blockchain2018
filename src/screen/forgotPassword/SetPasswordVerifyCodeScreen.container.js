import { connect } from 'react-redux';
import SetPasswordVerifyCodeScreen from './SetPasswordVerifyCodeScreen.component';

import { sendForgotPasswordEmail } from 'dike/store/user/user.actions';

export default connect(
  ({ app }) => ({
    isLoading: app.forgotPasswordScreen.isLoading
  }),
  dispatch => ({
    onResend(email) {
      dispatch(sendForgotPasswordEmail(email));
    }
  })
)(SetPasswordVerifyCodeScreen);
