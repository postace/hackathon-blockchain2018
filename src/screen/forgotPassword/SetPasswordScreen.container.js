import { connect } from 'react-redux';
import SetPasswordScreen from './SetPasswordScreen.component';

import { setPassword } from 'dike/store/user/user.actions';

export default connect(
  ({ app }) => ({
    isLoading: app.forgotPasswordScreen.isLoading,
    errorSetPassword: app.forgotPasswordScreen.errorSetPassword
  }),
  dispatch => ({
    onSubmit({ email, password, code }, onSuccess) {
      dispatch(setPassword({ email, password, code }, onSuccess));
    }
  })
)(SetPasswordScreen);
