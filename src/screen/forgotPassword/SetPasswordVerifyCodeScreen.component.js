import React from 'react';

import PinInputScreen from 'dike/components/PinInputScreen';
import { getStorageItem } from 'dike/utils/asyncStorage';

const description =
  'A verification code has been sent to your email. Please enter the code you received.';

export default ({ isLoading, navigation, onResend }) => (
  <PinInputScreen
    isLoading={isLoading}
    description={description}
    onResend={async () => {
      const userEmail = await getStorageItem('userEmail');
      onResend(userEmail);
    }}
    onCodeVerify={code => {
      navigation.goBack();
      navigation.state.params.onGoBack(code);
    }}
  />
);
