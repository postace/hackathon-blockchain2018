import * as sounds from 'dike/utils/sounds';
import React, { Component } from 'react';

import { FlatList } from 'react-native';
import { Content, ListItem, Left, Body, Right, Button, Icon, Text, Switch } from 'native-base';
import { setStorageItem, getStorageItem } from 'dike/utils/asyncStorage';

import ConfirmDialog from 'dike/components/ConfirmDialog';
import DikeBackground from 'dike/components/DikeBackground';
import UserInfo from 'dike/components/UserInfo';

const menuItems = [
  {
    type: 'switch',
    id: 'volume',
    label: 'Sound effects',
    iconName: 'ios-volume-mute',
    tintIcon: 'ios-volume-off'
  },
  { id: 'editProfile', label: 'Edit profile', iconName: 'ios-information' },
  { id: 'changePassword', label: 'Change password', iconName: 'ios-lock' },
  { id: 'editPrivacy', label: 'Privacy rule', iconName: 'ios-alert' },
  { id: 'reportIssue', label: 'Report issue', iconName: 'ios-warning' },
  { id: 'signOut', label: 'Sign out', iconName: 'ios-log-out' }
];

const NormalItem = ({ item, onPress }) => (
  <ListItem icon button onPress={() => onPress && onPress(item)}>
    <Left>
      <Button>
        <Icon active name={item.iconName} />
      </Button>
    </Left>
    <Body>
      <Text>{item.label}</Text>
    </Body>
    <Right>
      <Icon active name="ios-arrow-forward" />
    </Right>
  </ListItem>
);

const SwitchItem = ({ item, value, onSwitch }) => (
  <ListItem icon button onPress={() => onSwitch(!value)}>
    <Left>
      <Button>
        <Icon
          active
          name={value === true ? item.iconName : item.tintIcon ? item.tintIcon : item.iconName}
        />
      </Button>
    </Left>
    <Body>
      <Text>{item.label}</Text>
    </Body>
    <Right>
      <Switch value={value} onValueChange={v => onSwitch(v)} />
    </Right>
  </ListItem>
);

export default class SettingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isConfirmingSignOut: false,
      switchValues: { volume: false }
    };
    this.getUserConfigValues();
  }

  async getUserConfigValues() {
    const { switchValues } = this.state;
    let volume = await getStorageItem('volume');
    volume = parseInt(volume, 10);
    this.setState({
      switchValues: { ...switchValues, volume: volume === 1 }
    });
  }

  async handleItemSwitch(value, id) {
    const { switchValues } = this.state;
    switch (id) {
      case 'volume':
        for (let key in sounds) {
          sounds[key].setVolume(value === true ? 1 : 0);
        }
        await setStorageItem('volume', value === true ? '1' : '0');
        break;
      default:
        break;
    }
    this.setState({ switchValues: { ...switchValues, [id]: value } });
  }

  handleItemPress(id) {
    const { navigation } = this.props;
    switch (id) {
      case 'signOut':
        this.setState({ isConfirmingSignOut: true });
        return;
      case 'changePassword':
        navigation.navigate('ChangePassword');
        return;
      case 'reportIssue':
        navigation.navigate('ReportIssue');
        return;
      case 'editProfile':
        navigation.navigate('EditProfile');
        return;
      case 'editPrivacy':
        navigation.navigate('PrivacyScreen');
        return;
      default:
        return;
    }
  }

  render() {
    const { isConfirmingSignOut, switchValues } = this.state;
    const { onSignOut, navigation } = this.props;
    return (
      <DikeBackground>
        <Content>
          <UserInfo ViewProps={{ paddingContent: true }} />
          <FlatList
            data={menuItems}
            extraData={this.state}
            keyExtractor={item => item.id}
            renderItem={({ item }) => {
              switch (item.type) {
                case 'switch':
                  return (
                    <SwitchItem
                      item={item}
                      value={switchValues[item.id]}
                      onSwitch={v => this.handleItemSwitch(v, item.id)}
                    />
                  );
                default:
                  return <NormalItem item={item} onPress={() => this.handleItemPress(item.id)} />;
              }
            }}
          />
        </Content>
        {isConfirmingSignOut && (
          <ConfirmDialog
            onDismiss={() => this.setState({ isConfirmingSignOut: false })}
            onSubmit={() => {
              onSignOut();
              sounds.tap.play();
              navigation.popToTop();
            }}
            title="Are you sure?"
            notice="You are attempting to sign out of Dike."
          />
        )}
      </DikeBackground>
    );
  }
}
