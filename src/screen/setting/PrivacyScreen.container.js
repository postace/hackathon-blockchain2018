import { connect } from 'react-redux';
import PrivacyScreen from './PrivacyScreen.component';

import { updatePrivacyRule } from 'dike/store/user/user.actions';

export default connect(
  ({ user }) => ({
    allowMember: user.allowMember,
    allowService: user.allowService
  }),
  dispatch => ({
    updatePrivacyRule(allowMember, allowService) {
      dispatch(updatePrivacyRule(allowMember, allowService));
    }
  })
)(PrivacyScreen);
