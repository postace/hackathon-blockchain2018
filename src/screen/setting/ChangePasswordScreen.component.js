import { tap } from 'dike/utils/sounds';
import React, { Component } from 'react';

import { Form, View, Button, Text, Content, H1 } from 'native-base';

import DikeBackground from 'dike/components/DikeBackground';
import ItemInputPassword from 'dike/components/ItemInputPassword';
import UserInfo from 'dike/components/UserInfo';
import ConfirmDialog from 'dike/components/ConfirmDialog';
import ScreenOverlayLoading from 'dike/components/ScreenOverlayLoading';

import { validatePassword } from 'dike/utils/common/validators';

export default class ChangePasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      newPassword: '',
      error: {},
      isSuccess: false,
      isConfirming: false
    };
  }

  handleShowCofirmBox() {
    const { newPassword } = this.state;
    const isValidPassword = validatePassword(newPassword);
    if (!isValidPassword) {
      this.setState({ error: { newPassword: true } });
      return;
    }
    this.setState({ isConfirming: true, error: { newPassword: false } });
  }

  handleSubmit() {
    this.setState({ isConfirming: false });
    const { oldPassword, newPassword } = this.state;
    const { onChangePassword } = this.props;

    const formBody = { oldPassword, newPassword };
    onChangePassword(formBody, () => this.setState({ isSuccess: true }));
  }

  render() {
    const { oldPassword, newPassword, error, isSuccess, isConfirming } = this.state;
    const { navigation, errorSubmit, isLoading } = this.props;
    return (
      <DikeBackground>
        <Content>
          <UserInfo ViewProps={{ paddingContent: true }} />

          <View fullWidth center>
            {!isSuccess && (
              <View style={{ width: '80%' }} center>
                {!!errorSubmit && (
                  <Text small error>
                    *{errorSubmit}
                  </Text>
                )}

                <Form full>
                  <ItemInputPassword
                    placeholder="Current password"
                    value={oldPassword}
                    onChangeText={v => this.setState({ oldPassword: v })}
                  />
                  <ItemInputPassword
                    error={error.newPassword}
                    placeholder="New password"
                    value={newPassword}
                    onChangeText={v => {
                      const nextState = { newPassword: v };
                      if (error.newPassword) {
                        error.newPassword = !validatePassword(v);
                        nextState.error = error;
                      }
                      this.setState(nextState);
                    }}
                  />
                </Form>

                <View half fullFormFooter>
                  <Button
                    block
                    primary
                    onPress={() => {
                      this.handleShowCofirmBox();

                      tap.play();
                    }}>
                    <Text>Change</Text>
                  </Button>
                  <Button
                    block
                    info
                    onPress={() => {
                      navigation.goBack();
                      tap.play();
                    }}>
                    <Text>Cancel</Text>
                  </Button>
                </View>
              </View>
            )}
            {isSuccess && (
              <View style={{ width: '80%' }} center>
                <H1 bold>Success!</H1>
                <Text endDescription>Your password has been changed.</Text>
                <View fullWidth center>
                  <View half fullFormFooter>
                    <Button
                      info
                      block
                      onPress={() => {
                        navigation.goBack();
                        tap.play();
                      }}>
                      <Text>Back</Text>
                    </Button>
                  </View>
                </View>
              </View>
            )}
          </View>
        </Content>
        {isConfirming && (
          <ConfirmDialog
            onDismiss={() => this.setState({ isConfirming: false })}
            onSubmit={() => {
              this.handleSubmit();
              tap.play();
            }}
            title="Are you sure?"
            notice="You are attempting to change your password. Please, check it carefully!"
          />
        )}
        {isLoading && <ScreenOverlayLoading />}
      </DikeBackground>
    );
  }
}
