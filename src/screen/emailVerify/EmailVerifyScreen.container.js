import { connect } from 'react-redux';
import EmailVerifyScreen from './EmailVerifyScreen.component';

import { verifyEmail, setVerifyEmailError, resendVerifyCode } from 'dike/store/user/user.actions';

export default connect(
  ({ user, app }) => ({
    isLoading: app.isVerifyEmailScreenLoading,
    hasError: user.hasVerifyEmailError
  }),
  dispatch => ({
    onVerify(code) {
      dispatch(verifyEmail(code));
    },
    onHideError() {
      dispatch(setVerifyEmailError(false));
    },
    onResend() {
      dispatch(resendVerifyCode());
    }
  })
)(EmailVerifyScreen);
