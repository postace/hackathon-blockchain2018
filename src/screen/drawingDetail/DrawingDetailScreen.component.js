import React, { Component } from 'react';
import { Content } from 'native-base';

import DikeBackground from 'dike/components/DikeBackground';
import ScreenLoading from 'dike/components/ScreenLoading';

import RewardLeaderboardCard from './RewardLeaderboardCard.component';
import UserBetsResultCard from './UserBetsResultCard.component';
import ResultCard from './ResultCard.component';

export default class DrawingDetailScreen extends Component {
  componentDidMount() {
    const { navigation, onGetDrawing } = this.props;
    const drawingId = navigation.getParam('drawingId');
    onGetDrawing(drawingId);
  }

  render() {
    const { isLoading, drawing } = this.props;

    return (
      <DikeBackground>
        {isLoading && <ScreenLoading />}
        {!isLoading && (
          <Content>
            <ResultCard drawing={drawing} />
            {!!drawing.participated &&
              drawing.personalStats &&
              drawing.personalStats.rewardDetails && (
                <UserBetsResultCard
                  result={drawing.personalStats.rewardDetails}
                  totalReward={drawing.personalStats.totalReward}
                />
              )}
            {drawing.drawingStats.rewardLeaderboard.length > 0 && (
              <RewardLeaderboardCard leaderboard={drawing.drawingStats.rewardLeaderboard} />
            )}
          </Content>
        )}
      </DikeBackground>
    );
  }
}
