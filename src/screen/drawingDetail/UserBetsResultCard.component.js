import React from 'react';
import BigNumber from 'bignumber.js';

import { StyleSheet } from 'react-native';
import { View, Text, Icon, Card, CardItem, H3, Left, Right } from 'native-base';
import DikeNumber from 'dike/components/DikeNumber';

const styles = StyleSheet.create({
  winningItem: { width: `${100 / 3}%` },
  losingItem: { width: `${100 / 7}%` }
});

export default ({ result = [], totalReward = 0 }) => {
  const winningBets = result.filter(bet => bet.reward !== '0').sort(bet => !bet.special);
  const losingNormalBets = result
    .filter(bet => bet.reward === '0' && !bet.special)
    .sort((b1, b2) => b1.number - b2.number);
  const losingSpecialBets = result
    .filter(bet => bet.reward === '0' && bet.special)
    .sort((b1, b2) => b1.number - b2.number);
  const losingBets = losingSpecialBets.concat(losingNormalBets);

  if (winningBets.length < 1 && losingBets.length < 1) {
    return null;
  }

  return (
    <Card bgMain>
      <CardItem header borderBottom>
        <Left>
          <H3 bold>My Numbers</H3>
        </Left>
        <Right>
          <H3 important>Total reward: {new BigNumber(totalReward).toFormat(0)}</H3>
        </Right>
      </CardItem>
      <CardItem>
        <View fullWidth>
          {winningBets.length > 0 && (
            <View horizontal wrap>
              {winningBets.map((bet, i) => (
                <View wrapItem center style={styles.winningItem} key={i}>
                  <DikeNumber
                    special={!!bet.special}
                    number={bet.number}
                    size={60}
                    occurence={bet.occurence}
                  />
                  <View center horizontal>
                    <Text bold>{new BigNumber(bet.stake).toFormat(0)} </Text>
                    <Icon name="md-arrow-dropright" small />
                    <Text bold> {new BigNumber(bet.reward).toFormat(0)}</Text>
                  </View>
                </View>
              ))}
            </View>
          )}
          {losingBets.length > 0 && (
            <View horizontal wrap>
              {losingBets.map((bet, i) => (
                <View wrapItem center style={styles.losingItem} key={i}>
                  <DikeNumber special={!!bet.special} lost={true} number={bet.number} size={40} />
                  <Text blur bold>
                    {new BigNumber(bet.stake).toFormat(0)}
                  </Text>
                </View>
              ))}
            </View>
          )}
        </View>
      </CardItem>
    </Card>
  );
};
