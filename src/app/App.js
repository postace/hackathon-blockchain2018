import React from 'react';
import * as sounds from 'dike/utils/sounds';
import { setStorageItem, getStorageItem } from 'dike/utils/asyncStorage';

import { StyleProvider, Root } from 'native-base';
import getTheme from 'dike/native-base-theme/components';
import commonColor from 'dike/native-base-theme/variables/commonColor';

import { Provider } from 'react-redux';
import { store } from 'dike/store';

import AppState from './AppState.container';

const getDefaultConfig = async () => {
  let volume = await getStorageItem('volume');
  if (volume === null) {
    await setStorageItem('volume', '1');
    volume = '1';
  }
  volume = parseInt(volume, 10);
  for (let key in sounds) {
    sounds[key].setVolume(volume);
  }
};

getDefaultConfig();

export default props => (
  <Provider store={store}>
    <StyleProvider style={getTheme(commonColor)}>
      <Root>
        <AppState />
      </Root>
    </StyleProvider>
  </Provider>
);
