import { connect } from 'react-redux';
import AppStack from './AppStack.component';

export default connect(({ user }) => ({
  isLoggedIn: !!user.username,
  emailVerified: user.emailVerified
}))(AppStack);
