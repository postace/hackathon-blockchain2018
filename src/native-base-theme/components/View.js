import dikeViewTheme from 'dike/theme/components/View';
import variable from './../variables/platform';

export default (variables = variable) => {
  const viewTheme = {
    '.padder': {
      padding: variables.contentPadding
    },
    ...dikeViewTheme(variables)
  };

  return viewTheme;
};
